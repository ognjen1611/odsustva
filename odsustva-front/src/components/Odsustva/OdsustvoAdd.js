import React from "react";
import { withNavigation, withParams } from "../../routeconf";
import TestAxios from "../../apis/TestAxios";
import { Button, Form, FormControl, FormGroup, FormLabel } from "react-bootstrap";

class KomponentaAdd extends React.Component {

    constructor(props){
        super(props);

        let odsustvo = {
            datumPocetka: "",
            radnihDana: 0,
            radnikId: this.props.params.id
        }

        this.state = { 
            odsustvo: odsustvo,
            radnik: ""
        }
    }

    componentDidMount(){
        this.getRadnik()
    }

    async getRadnik(){
        try{
            let res = await TestAxios.get("/radnici/" + this.state.odsustvo.radnikId);
            if (res && res.status === 200){
                this.setState({
                    radnik: res.data
                })
            }

        }catch (error) {
            alert("Nije uspelo dobavljanje radnika.");
        }
    }

    valueInputChange(event) {
        let name = event.target.name;
        let value = event.target.value;

        let odsustvo = this.state.odsustvo;

        odsustvo[name] = value;

        console.log(name + " " + value)

        this.setState({ odsustvo : odsustvo });

        console.log(this.state.odsustvo)
    }

    async create(){
        try{
            let res = await TestAxios.post("/radnici/" + this.props.params.id + "/odsustva", this.state.odsustvo);
            if (res && res.status === 201){
                this.navigateRadnici();
            }
        } catch (err) {
            alert("Dodavanje odsustva nije uspelo!");
        }
    }

    navigateRadnici(){
        this.props.navigate("/radnici");
    }

    render(){

            if (this.state.radnik.slobodniDani === 0){
                return(
                    <h1>ISKORISTILI STE SVE SLOBODNE DANE</h1>
                )
            }
            return(
                <Form>
                    <FormGroup>
                        <FormLabel>
                            Datum pocetka
                        </FormLabel>
                        <FormControl    
                            name = "datumPocetka"
                            as = "input"
                            type="date"
                            onChange = {(e) => this.valueInputChange(e)}
                        ></FormControl>
                    </FormGroup>
                    <FormGroup>
                        <FormLabel>
                            Broj radnih dana
                        </FormLabel>
                        <FormControl
                            placeholder="Broj radnih dana"    
                            name = "radnihDana"
                            as = "input"
                            onChange = {(e) => this.valueInputChange(e)}
                            type = "number"
                            min = "0"
                            step = "1"
                        ></FormControl>
                    </FormGroup>

                    <Button style={{marginTop: "30px"}} onClick={() => this.create()}>Dodaj</Button>
                </Form>
            )
    }
}

export default withNavigation(withParams(KomponentaAdd));