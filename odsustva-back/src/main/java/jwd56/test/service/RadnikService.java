package jwd56.test.service;

import org.springframework.data.domain.Page;

import jwd56.test.model.Radnik;

public interface RadnikService {

	Radnik findOne(Long id);
	
	Page<Radnik> search(String jmbg, Long odeljenjeId, int pageNo);
	
	Radnik save(Radnik entitet);
	
	Radnik update(Radnik entitet);

	Radnik delete(Long id);
}
