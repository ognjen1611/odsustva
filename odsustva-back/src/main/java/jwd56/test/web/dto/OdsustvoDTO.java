package jwd56.test.web.dto;

import java.time.LocalDate;

public class OdsustvoDTO {
	
	private Long id;
	
	private LocalDate datumPocetka;
	
	private Integer radnihDana;
	
	private Long radnikId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDatumPocetka() {
		return datumPocetka;
	}

	public void setDatumPocetka(LocalDate datumPocetka) {
		this.datumPocetka = datumPocetka;
	}

	public Integer getRadnihDana() {
		return radnihDana;
	}

	public void setRadnihDana(Integer radnihDana) {
		this.radnihDana = radnihDana;
	}

	public Long getRadnikId() {
		return radnikId;
	}

	public void setRadnikId(Long radnikId) {
		this.radnikId = radnikId;
	}
}
