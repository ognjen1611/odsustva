package jwd56.test.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import jwd56.test.model.Radnik;
import jwd56.test.repository.RadnikRepository;
import jwd56.test.service.RadnikService;

@Service
public class JpaRadnikService implements RadnikService {

	@Autowired
	private RadnikRepository radnikRepository;
	
	@Override
	public Radnik findOne(Long id) {
		return radnikRepository.findOneById(id);
	}

	@Override
	public Page<Radnik> search(String jmbg, Long odeljenjeId, int pageNo) {
		if (jmbg == null) {
			jmbg = "";
		}
		
		if (odeljenjeId == null) {
			return radnikRepository.findByJmbgContains(jmbg, PageRequest.of(pageNo, 3));
		} else {
			return radnikRepository.findByJmbgContainsAndOdeljenjeId(jmbg, odeljenjeId, PageRequest.of(pageNo, 3));
		}
	}

	@Override
	public Radnik save(Radnik entitet) {
		return radnikRepository.save(entitet);
	}

	@Override
	public Radnik update(Radnik entitet) {
		return radnikRepository.save(entitet);
	}

	@Override
	public Radnik delete(Long id) {
		Optional<Radnik> opt = radnikRepository.findById(id);
		if (opt.isPresent()) {
			radnikRepository.deleteById(id);
			return opt.get();
		}
		return null;
	}
}
