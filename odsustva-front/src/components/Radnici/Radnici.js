import React from "react";
import { Button, ButtonGroup, Col, Form, FormControl, FormGroup, FormLabel, FormSelect, Row, Table } from "react-bootstrap";
import TestAxios from "../../apis/TestAxios";
import { withNavigation, withParams } from "../../routeconf";

class Radnici extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            radnici: [],
            odeljenja: [],
            search: { jmbg: "", odeljenjeId: -1 },
            pageNo: 0,
            totalPages: 1
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await this.getRadnici(0);
        await this.getOdeljenja();
    }

    async getRadnici(page) {

        let config = {
            params: {
                pageNo: page
            }
        }

        if (this.state.search.jmbg != "") {
            config.params.jmbg = this.state.search.jmbg;
        }

        if (this.state.search.odeljenjeId != -1) {
            config.params.odeljenjeId = this.state.search.odeljenjeId;
        }

        try {
            let res = await TestAxios.get("/radnici", config);
            if (res && res.status === 200) {

                this.setState({
                    pageNo: page,
                    totalPages: res.headers["total-pages"],
                    radnici: res.data,
                })
            }
        } catch (err) {
            console.log(err)
            alert("Nije uspelo dobavljanje radnika.");
        }
    }

    async getOdeljenja() {
        try {
            let res = await TestAxios.get("/odeljenja");
            if (res && res.status === 200) {
                this.setState({
                    odeljenja: res.data
                })
            }
        } catch (error) {
            alert("Nije uspelo dobavljanje odeljenja.");
        }
    }

    searchValueInputChange(event) {
        let name = event.target.name;
        let value = event.target.value;

        let search = this.state.search;
        search[name] = value;

        this.setState({ search: search });

        this.getRadnici(0);
    }

    async delete(radnikId) {
        try {
            let res = await TestAxios.delete("/radnici/" + radnikId);

            if (res && res.status === 204) {
                let nextPage;
                if (this.state.pageNo == this.state.totalPages - 1 && this.state.radnici.length == 1 && this.state.pageNo != 0) {
                    nextPage = this.state.pageNo - 1
                } else {
                    nextPage = this.state.pageNo
                }

                await this.getRadnici(nextPage);
            }
        } catch (error) {
            alert("Nije uspelo brisanje radnika.");
        }
    }

    goToAdd() {
        this.props.navigate("/radnici/add");
    }

    goToOdsustvo(id) {
        this.props.navigate("/radnici/" + id + "/odsustvo/add");
    }

    render() {
        return <>

            <h1>Radnici</h1>

            <br />

            <h3>Pretraga</h3>
            <Form>
                <FormGroup>
                    <FormLabel>
                        Odeljenje
                    </FormLabel>
                    <FormSelect
                        onChange={(e) => this.searchValueInputChange(e)}
                        name="odeljenjeId"
                        value={this.state.search.odeljenjeId}
                    >
                        <option value={-1}></option>
                        {this.state.odeljenja.map((ent) => {
                            return (
                                <option value={ent.id} key={ent.id}>
                                    {ent.naziv}
                                </option>
                            )
                        })}
                    </FormSelect>
                </FormGroup>
                <FormGroup>
                    <FormLabel>
                        JMBG
                    </FormLabel>
                    <FormControl
                        placeholder="Unesite JMBG"
                        value={this.state.search.jmbg}
                        name="jmbg"
                        onChange={(e) => this.searchValueInputChange(e)}
                    ></FormControl>
                </FormGroup>
            </Form>

            <br />
            <br />

            {window.localStorage['role'] == "ROLE_ADMIN" ?
                <Button onClick={() => this.goToAdd()}>Dodajte radnika</Button>
                : null}

            <br />
            <br />
            <br />

            <ButtonGroup style={{ marginTop: 5, float: "right" }}>
                <Button
                    style={{ marginBottom: 3, backgroundColor: "#39a2bd", border: "none" }}
                    disabled={this.state.pageNo == 0} onClick={() => this.getRadnici(this.state.pageNo - 1)}>
                    Prethodna
                </Button>
                <Button
                    style={{ marginBottom: 3, backgroundColor: "#39a2bd", border: "none" }}
                    disabled={this.state.pageNo >= this.state.totalPages - 1} onClick={() => this.getRadnici(this.state.pageNo + 1)}>
                    Sledeca
                </Button>
            </ButtonGroup>

            <Table striped hover style={{ marginTop: 15 }}>
                <thead style={{ backgroundColor: "black", color: "white" }}>
                    <tr>
                        <th>JMBG</th>
                        <th>Ime i prezime</th>
                        <th>Email</th>
                        <th>Slobodni dani</th>
                        <th>Odeljenje</th>
                        <th colSpan={2}>Akcije</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.radnici.map((radnik, index) => {
                        return (
                            <tr key={radnik.id}>
                                <td>{radnik.jmbg}</td>
                                <td>{radnik.imePrezime}</td>
                                <td>{radnik.email}</td>
                                <td>{radnik.slobodniDani}</td>
                                <td>{radnik.odeljenjeNaziv}</td>

                                <td>
                                    <Button
                                        name="dodajOdsustvo"
                                        style={{ marginRight: "10px" }}
                                        variant="primary"
                                        onClick={() => this.goToOdsustvo(radnik.id)}>
                                        Odsustvo
                                    </Button>

                                    <Button class="btn btn-secondary" onClick={() => this.delete(radnik.id)} variant="danger">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                        </svg>
                                    </Button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
        </>
    }
}

export default withNavigation(withParams(Radnici));