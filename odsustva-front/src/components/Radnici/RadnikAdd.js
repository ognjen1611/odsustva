import React from "react";
import { withNavigation } from "../../routeconf";
import TestAxios from "../../apis/TestAxios";
import { Button, Form, FormControl, FormGroup, FormSelect, FormLabel } from "react-bootstrap";

class RadnikAdd extends React.Component {

    constructor(props){
        super(props);

        let radnik = {
            jmbg: "",
            imePrezime: "",
            email: "",
            godineStaza: "",
            odeljenjeId: -1
        }

        this.state = { 
            radnik: radnik,
            odeljenja : []         
        }
    }

    componentDidMount(){
        this.getOdeljenja()
    }

    async getOdeljenja(){
        try{
            let res = await TestAxios.get("/odeljenja");
            if (res && res.status === 200){
                this.setState({
                    odeljenja: res.data
                })
            }
        }catch (error) {
            alert("Nije uspelo dobavljanje odeljenja.");
        }
    }

    valueInputChange(event) {
        let name = event.target.name;
        let value = event.target.value;

        let radnik = this.state.radnik;

        radnik[name] = value;

        this.setState({ radnik : radnik });
    }

    async create(){
        try{
            let res = await TestAxios.post("/radnici", this.state.radnik);
            if (res && res.status === 201){
                this.navigateRadnici();
            }
        } catch (err) {
            alert("Dodavanje radnika nije uspelo!");
        }
    }

    navigateRadnici(){
        this.props.navigate("/radnici");
    }

    render(){
            return(
                <Form>
                    <FormGroup>
                        <FormLabel>
                            JMBG
                        </FormLabel>
                        <FormControl
                            placeholder="JMBG"    
                            name = "jmbg"
                            as = "input"
                            onChange = {(e) => this.valueInputChange(e)}
                        ></FormControl>
                    </FormGroup>
                    <FormGroup>
                        <FormLabel>
                            Ime i prezime
                        </FormLabel>
                        <FormControl
                            placeholder="Ime i prezime"    
                            name = "imePrezime"
                            as = "input"
                            onChange = {(e) => this.valueInputChange(e)}
                        ></FormControl>
                    </FormGroup>
                    <FormGroup>
                        <FormLabel>
                            E-mail
                        </FormLabel>
                        <FormControl
                            placeholder="E-mail"    
                            name = "email"
                            as = "input"
                            type="email"
                            onChange = {(e) => this.valueInputChange(e)}
                        ></FormControl>
                    </FormGroup>
                    <FormGroup>
                        <FormLabel>
                            Godine radnog staza
                        </FormLabel>
                        <FormControl
                            placeholder="Godine radnog staza"    
                            name = "godineStaza"
                            as = "input"
                            onChange = {(e) => this.valueInputChange(e)}
                            type = "number"
                            min = "0"
                            step = "1"
                        ></FormControl>
                    </FormGroup>
                    <FormGroup>
                        <FormLabel>
                            Odeljenje
                        </FormLabel>
                        <FormSelect
                            onChange = {(e) => this.valueInputChange(e)}
                            name = "odeljenjeId"
                        >
                            <option value={-1}></option>
                            {this.state.odeljenja.map((sel) => {
                                return (
                                    <option value={sel.id} key={sel.id}>
                                        {sel.naziv}
                                    </option>
                                )
                            })}
                        </FormSelect>
                    </FormGroup>

                    <Button style={{marginTop: "30px"}} onClick={() => this.create()}>Dodaj</Button>
                </Form>
            )
    }
}

export default withNavigation(RadnikAdd);