package jwd56.test.support;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd56.test.model.Odsustvo;
import jwd56.test.web.dto.OdsustvoDTO;

@Component
public class OdsustvoToOdsustvoDto implements Converter<Odsustvo, OdsustvoDTO> {

	@Override
	public OdsustvoDTO convert(Odsustvo source) {
		OdsustvoDTO dto = new OdsustvoDTO();
		
		dto.setId(source.getId());
		dto.setDatumPocetka(source.getDatumPocetka());
		dto.setRadnihDana(source.getRadnihDana());
		dto.setRadnikId(source.getRadnik().getId());

		return dto;
	}
	
	public List<OdsustvoDTO> convert(List<Odsustvo> lista){
		return lista.stream().map(this::convert).collect(Collectors.toList());
	}
}
