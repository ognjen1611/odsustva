package jwd56.test.model;

import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Odsustvo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private LocalDate datumPocetka;
	
	@Column(nullable = false)
	private Integer radnihDana;
	
	@ManyToOne
	private Radnik radnik;

	public Odsustvo() {
		super();
	}

	public Odsustvo(Long id, LocalDate datumPocetka, Integer radnihDana, Radnik radnik) {
		super();
		this.id = id;
		this.datumPocetka = datumPocetka;
		this.radnihDana = radnihDana;
		this.radnik = radnik;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Odsustvo other = (Odsustvo) obj;
		return Objects.equals(id, other.id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDatumPocetka() {
		return datumPocetka;
	}

	public void setDatumPocetka(LocalDate datumPocetka) {
		this.datumPocetka = datumPocetka;
	}

	public Integer getRadnihDana() {
		return radnihDana;
	}

	public void setRadnihDana(Integer radnihDana) {
		this.radnihDana = radnihDana;
	}

	public Radnik getRadnik() {
		return radnik;
	}

	public void setRadnik(Radnik radnik) {
		this.radnik = radnik;
	}
	
	
}
