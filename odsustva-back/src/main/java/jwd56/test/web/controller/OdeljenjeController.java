package jwd56.test.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jwd56.test.model.Odeljenje;
import jwd56.test.service.OdeljenjeService;
import jwd56.test.support.OdeljenjeToOdeljenjeDto;
import jwd56.test.web.dto.OdeljenjeDTO;

@RestController
@RequestMapping(value = "/api/odeljenja", produces = MediaType.APPLICATION_JSON_VALUE)
public class OdeljenjeController {
	
	@Autowired
	private OdeljenjeToOdeljenjeDto toDto;
	
	@Autowired
	private OdeljenjeService service;
	
	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping
    public ResponseEntity<List<OdeljenjeDTO>> get(){

        List<Odeljenje> rezultat = service.findAll();

        return new ResponseEntity<>(toDto.convert(rezultat), HttpStatus.OK);
    }

    @ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Void> handle() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
