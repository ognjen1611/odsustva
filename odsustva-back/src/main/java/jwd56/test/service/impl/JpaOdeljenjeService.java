package jwd56.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd56.test.model.Odeljenje;
import jwd56.test.repository.OdeljenjeRepository;
import jwd56.test.service.OdeljenjeService;

@Service
public class JpaOdeljenjeService implements OdeljenjeService {
	
	@Autowired
	private OdeljenjeRepository repository;

	@Override
	public Odeljenje findOne(Long id) {
		return repository.findOneById(id);
	}

	@Override
	public List<Odeljenje> findAll() {
		return repository.findAll();
	}

}
