package jwd56.test.support;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd56.test.model.Radnik;
import jwd56.test.web.dto.RadnikDTO;

@Component
public class RadnikToRadnikDto implements Converter<Radnik, RadnikDTO> {

	@Override
	public RadnikDTO convert(Radnik source) {
		RadnikDTO dto = new RadnikDTO();
		
		dto.setId(source.getId());
		dto.setJmbg(source.getJmbg());
		dto.setImePrezime(source.getImePrezime());
		dto.setEmail(source.getEmail());
		dto.setGodineStaza(source.getGodineStaza());
		dto.setSlobodniDani(source.getSlobodniDani());
		dto.setOdeljenjeId(source.getOdeljenje().getId());
		dto.setOdeljenjeNaziv(source.getOdeljenje().getNaziv());

		return dto;
	}
	
	public List<RadnikDTO> convert(List<Radnik> lista){
		return lista.stream().map(this::convert).collect(Collectors.toList());
	}
}
