package jwd56.test.web.dto;

import javax.validation.constraints.Email;

import org.hibernate.validator.constraints.Length;

public class RadnikDTO {

	private Long id;
	
	@Length(min = 13, max = 13)
	private String jmbg;
	
	private String imePrezime;
	
	@Email
	private String email;
	
	private Integer godineStaza;
	
	private Integer slobodniDani;
	
	private Long odeljenjeId;
	
	private String odeljenjeNaziv;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getImePrezime() {
		return imePrezime;
	}

	public void setImePrezime(String imePrezime) {
		this.imePrezime = imePrezime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getGodineStaza() {
		return godineStaza;
	}

	public void setGodineStaza(Integer godineStaza) {
		this.godineStaza = godineStaza;
	}

	public Integer getSlobodniDani() {
		return slobodniDani;
	}

	public void setSlobodniDani(Integer slobodniDani) {
		this.slobodniDani = slobodniDani;
	}

	public Long getOdeljenjeId() {
		return odeljenjeId;
	}

	public void setOdeljenjeId(Long odeljenjeId) {
		this.odeljenjeId = odeljenjeId;
	}

	public String getOdeljenjeNaziv() {
		return odeljenjeNaziv;
	}

	public void setOdeljenjeNaziv(String odeljenjeNaziv) {
		this.odeljenjeNaziv = odeljenjeNaziv;
	}

	
	
}
