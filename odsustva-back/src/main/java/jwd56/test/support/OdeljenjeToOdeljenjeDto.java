package jwd56.test.support;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd56.test.model.Odeljenje;
import jwd56.test.web.dto.OdeljenjeDTO;

@Component
public class OdeljenjeToOdeljenjeDto implements Converter<Odeljenje, OdeljenjeDTO> {

	@Override
	public OdeljenjeDTO convert(Odeljenje source) {
		OdeljenjeDTO dto = new OdeljenjeDTO();
		
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		return dto;
	}
	
	public List<OdeljenjeDTO> convert(List<Odeljenje> lista){
		return lista.stream().map(this::convert).collect(Collectors.toList());
	}
}
