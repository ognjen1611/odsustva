package jwd56.test.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd56.test.model.Odsustvo;
import jwd56.test.model.Radnik;
import jwd56.test.service.OdsustvoService;
import jwd56.test.service.RadnikService;
import jwd56.test.support.OdsustvoDtoToOdsustvo;
import jwd56.test.support.OdsustvoToOdsustvoDto;
import jwd56.test.support.RadnikDtoToRadnik;
import jwd56.test.support.RadnikToRadnikDto;
import jwd56.test.web.dto.OdsustvoDTO;
import jwd56.test.web.dto.RadnikDTO;

@RestController
@RequestMapping(value = "/api/radnici", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class RadnikController {
	
	@Autowired
	private RadnikDtoToRadnik fromDto;

	@Autowired
	private RadnikToRadnikDto toDto;
	
	@Autowired
	private OdsustvoDtoToOdsustvo toOdsustvo;
	
	@Autowired
	private OdsustvoToOdsustvoDto toOdsustvoDto;
	
	@Autowired
	private RadnikService service;
	
	@Autowired
	private OdsustvoService odsustvoService;
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RadnikDTO> create(@Valid @RequestBody RadnikDTO dto){
		Radnik entitet = fromDto.convert(dto);
		Radnik vrati = service.save(entitet);
		
		return new ResponseEntity<>(toDto.convert(vrati), HttpStatus.CREATED);
	}
	

	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping
	public ResponseEntity<List<RadnikDTO>> getAll(
			@RequestParam(required = false) String jmbg,
			@RequestParam(required = false) Long odeljenjeId,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo
			){
		
		Page<Radnik> rezultati = service.search(jmbg, odeljenjeId, pageNo);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(rezultati.getTotalPages()));
		return new ResponseEntity<>(toDto.convert(rezultati.getContent()), headers, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<RadnikDTO> getOne(@PathVariable Long id){
		Radnik entitet = service.findOne(id);
		
		if (entitet != null) {
			return new ResponseEntity<>(toDto.convert(entitet), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RadnikDTO> update(@PathVariable Long id, @Valid @RequestBody RadnikDTO dto){
		
		if (!id.equals(dto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Radnik entitet = fromDto.convert(dto);
		Radnik vrati = service.update(entitet);
		
		return new ResponseEntity<>(toDto.convert(vrati), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id){
		
		Radnik obrisan = service.delete(id);
		
		if (obrisan != null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping(value = "/{id}/odsustva", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OdsustvoDTO> create(@RequestBody OdsustvoDTO dto){
		Odsustvo entitet = toOdsustvo.convert(dto);
		
		if ((entitet.getRadnik().getSlobodniDani() - dto.getRadnihDana()) < 0) {
			return new ResponseEntity<>(HttpStatus.I_AM_A_TEAPOT);
		}
		
		Odsustvo vrati = odsustvoService.save(entitet);
		
		return new ResponseEntity<>(toOdsustvoDto.convert(vrati), HttpStatus.CREATED);
	}
	
    @ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Void> handle() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
