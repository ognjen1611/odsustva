package jwd56.test.service;

import java.util.List;

import jwd56.test.model.Odsustvo;

public interface OdsustvoService {

	Odsustvo findOne(Long id);
	
	List<Odsustvo> findAll();

	Odsustvo save(Odsustvo entitet);

}
