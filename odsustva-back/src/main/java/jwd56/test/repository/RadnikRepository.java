package jwd56.test.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd56.test.model.Radnik;

@Repository
public interface RadnikRepository extends JpaRepository<Radnik, Long> {
	
	Radnik findOneById(Long id);
	
	Page<Radnik> findByJmbgContainsAndOdeljenjeId(String jmbg, Long odeljenjeId, Pageable pageable);
	
	Page<Radnik> findByJmbgContains(String jmbg, Pageable pageable);

}