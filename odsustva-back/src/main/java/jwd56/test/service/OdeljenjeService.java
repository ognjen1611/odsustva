package jwd56.test.service;

import java.util.List;

import jwd56.test.model.Odeljenje;

public interface OdeljenjeService {

	Odeljenje findOne(Long id);
	
	List<Odeljenje> findAll();

}
