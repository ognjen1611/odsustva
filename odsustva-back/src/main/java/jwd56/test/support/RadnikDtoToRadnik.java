package jwd56.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd56.test.model.Odeljenje;
import jwd56.test.model.Radnik;
import jwd56.test.service.OdeljenjeService;
import jwd56.test.service.RadnikService;
import jwd56.test.web.dto.RadnikDTO;

@Component
public class RadnikDtoToRadnik implements Converter<RadnikDTO, Radnik> {

	@Autowired
	private RadnikService service;
	
	@Autowired
	private OdeljenjeService odeljenjeService;
	
	@Override
	public Radnik convert(RadnikDTO dto) {
		
		Radnik entitet;
		
		if (dto.getId() == null) {
			entitet = new Radnik();
		} else {
			entitet = service.findOne(dto.getId());
		}
		
		if (entitet != null) {
			entitet.setJmbg(dto.getJmbg());
			entitet.setImePrezime(dto.getImePrezime());
			entitet.setEmail(dto.getEmail());
			entitet.setGodineStaza(dto.getGodineStaza());
			
			Odeljenje odeljenje = odeljenjeService.findOne(dto.getOdeljenjeId());
			entitet.setOdeljenje(odeljenje);

		}
		return entitet;
	}

}