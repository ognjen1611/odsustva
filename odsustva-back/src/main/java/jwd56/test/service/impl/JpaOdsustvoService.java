package jwd56.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd56.test.model.Odsustvo;
import jwd56.test.repository.OdsustvoRepository;
import jwd56.test.service.OdsustvoService;

@Service
public class JpaOdsustvoService implements OdsustvoService {
	
	@Autowired
	private OdsustvoRepository repository;

	@Override
	public Odsustvo findOne(Long id) {
		return repository.findOneById(id);
	}

	@Override
	public List<Odsustvo> findAll() {
		return repository.findAll();
	}

	@Override
	public Odsustvo save(Odsustvo entitet) {
		return repository.save(entitet);
	}
}
