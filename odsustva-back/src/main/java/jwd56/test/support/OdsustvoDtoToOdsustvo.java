package jwd56.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd56.test.model.Odsustvo;
import jwd56.test.model.Radnik;
import jwd56.test.service.OdsustvoService;
import jwd56.test.service.RadnikService;
import jwd56.test.web.dto.OdsustvoDTO;

@Component
public class OdsustvoDtoToOdsustvo implements Converter<OdsustvoDTO, Odsustvo> {

	@Autowired
	private OdsustvoService service;
	
	@Autowired
	private RadnikService radnikService;
	
	@Override
	public Odsustvo convert(OdsustvoDTO dto) {
		
		Odsustvo entitet;
		
		if (dto.getId() == null) {
			entitet = new Odsustvo();
		} else {
			entitet = service.findOne(dto.getId());
		}
		
		if (entitet != null) {
			entitet.setDatumPocetka(dto.getDatumPocetka());
			entitet.setRadnihDana(dto.getRadnihDana());
			Radnik radnik = radnikService.findOne(dto.getRadnikId());
			entitet.setRadnik(radnik);
		}
		return entitet;
	}

}