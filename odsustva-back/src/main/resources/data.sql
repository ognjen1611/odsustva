INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
              
INSERT INTO `odsustva`.`odeljenje` (`id`, `bonus`, `naziv`) VALUES ('1', '5', 'Teren');
INSERT INTO `odsustva`.`odeljenje` (`id`, `bonus`, `naziv`) VALUES ('2', '3', 'Kancelarija');
INSERT INTO `odsustva`.`odeljenje` (`id`, `bonus`, `naziv`) VALUES ('3', '2', 'Logistika');
INSERT INTO `odsustva`.`odeljenje` (`id`, `bonus`, `naziv`) VALUES ('4', '3', 'Call centar');

INSERT INTO `odsustva`.`radnik` (`id`, `email`, `godine_staza`, `ime_prezime`, `jmbg`, `odeljenje_id`) VALUES ('1', 'art@art.com', '13', 'Milica Mandic', '3456765434567', '1');
INSERT INTO `odsustva`.`radnik` (`id`, `email`, `godine_staza`, `ime_prezime`, `jmbg`, `odeljenje_id`) VALUES ('2', 'fart@art.com', '27', 'Novak Djokovic', '6865564564564', '2');
INSERT INTO `odsustva`.`radnik` (`id`, `email`, `godine_staza`, `ime_prezime`, `jmbg`, `odeljenje_id`) VALUES ('3', 'mart@art.com', '3', 'Pera Zdera', '4958375049382', '2');
INSERT INTO `odsustva`.`radnik` (`id`, `email`, `godine_staza`, `ime_prezime`, `jmbg`, `odeljenje_id`) VALUES ('4', 'kart@art.com', '15', 'Simbad Moreplovac', '5865987002480', '3');
INSERT INTO `odsustva`.`radnik` (`id`, `email`, `godine_staza`, `ime_prezime`, `jmbg`, `odeljenje_id`) VALUES ('5', 'lart@art.com', '25', 'Djordje Kapibara', '9158520369801', '3');
INSERT INTO `odsustva`.`radnik` (`id`, `email`, `godine_staza`, `ime_prezime`, `jmbg`, `odeljenje_id`) VALUES ('6', 'dart@art.com', '40', 'Mile Bjelobrk', '7852036000350', '4');

INSERT INTO `odsustva`.`odsustvo` (`id`, `datum_pocetka`, `radnih_dana`, `radnik_id`) VALUES ('1', '2022-03-13', '3', '2');
INSERT INTO `odsustva`.`odsustvo` (`id`, `datum_pocetka`, `radnih_dana`, `radnik_id`) VALUES ('2', '2022-03-20', '2', '2');
INSERT INTO `odsustva`.`odsustvo` (`id`, `datum_pocetka`, `radnih_dana`, `radnik_id`) VALUES ('3', '2022-02-24', '10', '4');
INSERT INTO `odsustva`.`odsustvo` (`id`, `datum_pocetka`, `radnih_dana`, `radnik_id`) VALUES ('4', '2022-03-13', '13', '5');