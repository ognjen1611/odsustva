package jwd56.test.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Radnik {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(unique = true, nullable = false)
	private String jmbg;
	
	@Column(nullable = false)
	private String imePrezime;
	
	private String email;
	
	private Integer godineStaza;
	
	@Transient
	private Integer slobodniDani;
	
	@ManyToOne
	private Odeljenje odeljenje;
	
	@OneToMany(mappedBy = "radnik", cascade = CascadeType.REMOVE)
	private List<Odsustvo> odsustva = new ArrayList<Odsustvo>();

	public Radnik(Long id, String jmbg, String imePrezime, String email, Integer godineStaza, Integer slobodniDani,
			Odeljenje odeljenje, List<Odsustvo> odsustva) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.imePrezime = imePrezime;
		this.email = email;
		this.godineStaza = godineStaza;
		this.slobodniDani = slobodniDani;
		this.odeljenje = odeljenje;
		this.odsustva = odsustva;
	}

	public Radnik() {
		super();
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Radnik other = (Radnik) obj;
		return Objects.equals(id, other.id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getImePrezime() {
		return imePrezime;
	}

	public void setImePrezime(String imePrezime) {
		this.imePrezime = imePrezime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getGodineStaza() {
		return godineStaza;
	}

	public void setGodineStaza(Integer godineStaza) {
		this.godineStaza = godineStaza;
	}

	public Integer getSlobodniDani() {
		Integer odsustva = this.odsustva.stream().map(Odsustvo::getRadnihDana).collect(Collectors.summingInt(Integer::intValue));
		Integer slobodniDani = 20 + this.godineStaza/5 + this.odeljenje.getBonus() - odsustva;
		return slobodniDani;
	}

	public void setSlobodniDani(Integer slobodniDani) {
		this.slobodniDani = slobodniDani;
	}

	public Odeljenje getOdeljenje() {
		return odeljenje;
	}

	public void setOdeljenje(Odeljenje odeljenje) {
		this.odeljenje = odeljenje;
	}

	public List<Odsustvo> getOdsustva() {
		return odsustva;
	}

	public void setOdsustva(List<Odsustvo> odsustva) {
		this.odsustva = odsustva;
	}
		
	
}
